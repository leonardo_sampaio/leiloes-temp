import { Center, Heading } from "@chakra-ui/react";

const Index = () => {
  return (
    <Center flex="1">
      <Heading>Back Office Index page</Heading>
    </Center>
  );
};

export default Index;
