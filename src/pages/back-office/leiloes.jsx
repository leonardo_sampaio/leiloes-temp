import { useEffect, useState } from "react";
import { Flex, Heading, List, ListItem } from "@chakra-ui/react";

import { useLeiloes } from "../../hooks/back-office/useLeiloes";
import { useUser } from "../../hooks/useUser";
import Custom401 from "../401";

const Leiloes = () => {
  const { userSession } = useUser();
  const { lotes } = useLeiloes();

  if (!userSession) return <Custom401 />;
  return (
    <Flex flex="1" flexDirection="column" align="center" justify="center">
      <Heading>Leilões BO</Heading>
      {lotes.length > 0 && (
        <>
          <Heading size="sm">Lotes</Heading>
          <List>
            {lotes.map((lote) => (
              <ListItem>{lote}</ListItem>
            ))}
          </List>
        </>
      )}
    </Flex>
  );
};

export default Leiloes;
