import React, { useState, useEffect } from "react";
import { VStack, Center, Heading, Button } from "@chakra-ui/react";
import { WarningIcon } from "@chakra-ui/icons";

import PasswordModal from "../components/PasswordModal.jsx";

const Custom401 = ({ text }) => {
  const [displayPasswordModal, setDisplayPasswordModal] = useState(false);
  useEffect(() => {
    if (displayPasswordModal) setDisplayPasswordModal(false);
  }, [displayPasswordModal]);
  return (
    <Center flex="1">
      <VStack>
        <PasswordModal open={displayPasswordModal} />
        <Heading size="lg">
          <WarningIcon mr="4" />
          Faça login para acessar este conteúdo
        </Heading>
        <Button
          onClick={() => setDisplayPasswordModal(true)}
          colorScheme="blue"
        >
          Login
        </Button>
      </VStack>
    </Center>
  );
};

export default Custom401;
