import {
  Container,
  Flex,
  Stack,
  Spacer,
  Box,
  Link,
  Button,
  Text,
  Image,
  Heading,
  FormControl,
  FormLabel,
  Input,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
  StatArrow,
  StatGroup,
  AspectRatio,
} from "@chakra-ui/react";
import useAnimateNumber from "use-animate-number";

const abbreviate_number = function (num, fixed) {
  if (num === null) {
    return null;
  } // terminate early
  if (num === 0) {
    return "0";
  } // terminate early
  fixed = !fixed || fixed < 0 ? 0 : fixed; // number of decimal places to show
  var b = num.toPrecision(2).split("e"), // get power
    k = b.length === 1 ? 0 : Math.floor(Math.min(b[1].slice(1), 14) / 3), // floor at decimals, ceiling at trillions
    c =
      k < 1
        ? num.toFixed(0 + fixed)
        : (num / Math.pow(10, k * 3)).toFixed(1 + fixed), // divide by power
    d = c < 0 ? c : Math.abs(c), // enforce -0 is 0
    e = d + ["", " Mil", " Milhões", " Bilhões", "T"][k]; // append power
  return e;
};

const Login = () => {
  let [bigNumber1, setBigNumber1] = useAnimateNumber(321909000, {
    duration: 7000,
    decimals: 0,
  });
  let [bigNumber2, setBigNumber2] = useAnimateNumber(19100000, {
    duration: 5000,
    decimals: 0,
  });
  let [bigNumber3, setBigNumber3] = useAnimateNumber(101136000, {
    duration: 6000,
    decimals: 0,
  });
  let [bigNumber4, setBigNumber4] = useAnimateNumber(2223000, {
    duration: 3000,
    decimals: 0,
  });
  let [bigNumber5, setBigNumber5] = useAnimateNumber(11000000, {
    duration: 4000,
    decimals: 0,
  });
  let [bigNumber6, setBigNumber6] = useAnimateNumber(400000000, {
    duration: 8000,
    decimals: 0,
  });

  bigNumber1 = abbreviate_number(bigNumber1, 0);
  bigNumber2 = abbreviate_number(bigNumber2, 0);
  bigNumber3 = abbreviate_number(bigNumber3, 0);
  bigNumber4 = abbreviate_number(bigNumber4, 0);
  bigNumber5 = abbreviate_number(bigNumber5, 0);
  bigNumber6 = abbreviate_number(bigNumber6, 0);

  return (
    <Flex
      flex="1"
      bgImage="/images/feno.jpg"
      bgRepeat="no-repeat"
      bgSize="cover"
      bgPosition="center"
    >
      <Container
        display="flex"
        flex="1"
        maxW={["sm", null, null, "container.xl"]}
        px={[0]}
      >
        <Flex flex="1" maxW="480" align={[0, null, "center"]}>
          <Box
            w="full"
            // m="0"
            mx={[0, null, null, 10]}
            px={[4, null, null, 10]}
            py={[4, null, null, 10]}
            bg={["gray.100", null, null, "rgba(255,255,255,0.75)"]}
            boxShadow={[0, 0, null, "dark-lg"]}
            rounded={[0, 0, "base", "2xl"]}
          >
            <Image
              src="/images/agromentoria-logo-h-tr.png"
              // maxW={["80%"]}
              // mx={["auto"]}
              // p="4"
              borderRadius="md"
              border="md"
            />
            <Heading
              fontSize={["xl"]}
              fontWeight="extrabold"
              textAlign={["center"]}
              mt={["4", null, null, "10"]}
            >
              Faça login para continuar
            </Heading>
            <Text
              textAlign={["center"]}
              fontSize={["lg"]}
              fontWeight="medium"
              color="gray.700"
              mt="6"
            >
              Não possui cadastro?
            </Text>
            <Text
              textAlign={["center"]}
              fontSize={["lg"]}
              fontWeight="medium"
              color="gray.700"
              mt="0"
            >
              <Link href="#" color="blue.500">
                Cadastrar
              </Link>
            </Text>
            <Box
              px={[4, null, null, 0]}
              py={[8, null, null, 0]}
              bg={["white", null, null, "transparent"]}
              rounded={["sm", null, null, "none"]}
              boxShadow={["base", null, null, "none"]}
              mt="8"
            >
              <FormControl id="email">
                <FormLabel>Email</FormLabel>
                <Input type="email" bg="white" />
              </FormControl>
              <FormControl id="password" mt="4">
                <FormLabel>Senha</FormLabel>
                <Input type="password" bg="white" />
              </FormControl>
              <Button
                type="submit"
                colorScheme="blue"
                size="lg"
                w="full"
                mt="6"
                fontWeight="normal"
              >
                Entrar
              </Button>
            </Box>
            <Link href="#" d="block" mt="8" textAlign="center" color="blue.500">
              Esqueci minha senha
            </Link>
          </Box>
        </Flex>
        <Flex
          display={["none", null, null, "flex"]}
          // my="10"
          direction="column"
          justify="center"
          flex="1"
        >
          <Flex
            p={[null, null, null, "4", "10"]}
            bg="transparent"
            rounded="base"
            direction="column"
          >
            <Heading
              fontSize={{ lg: "2xl", xl: "4xl" }}
              align="center"
              color="white"
            >
              Assessoria de excelência para o agro
            </Heading>
            <StatGroup
              // as="flex"
              color="white"
              mt="2"
              align="stretch"
              justify="center"
            >
              <Stat m="0" p="2" flexBasis={[null, null, null, "50%", "33%"]}>
                <StatLabel fontSize="lg" fontWeight="bold">
                  Recuperação Judicial e Administrativa
                </StatLabel>
                <StatNumber whiteSpace="nowrap" color="blue.200">
                  R$ {bigNumber6}
                </StatNumber>
                <StatHelpText color="gray.200">
                  Benefícios alcançados em recuperação judicial e
                  administrativa.
                </StatHelpText>
              </Stat>

              <Stat m="0" p="2" flexBasis={[null, null, null, "50%", "33%"]}>
                <StatLabel fontSize="lg" fontWeight="bold">
                  Aquisição de fazendas
                </StatLabel>
                <StatNumber whiteSpace="nowrap" color="blue.200">
                  R$ {bigNumber3}
                </StatNumber>
                <StatHelpText color="gray.200">
                  Transacionados em gestão de aquisição e assunção de direitos
                  imobiliários rurais.
                </StatHelpText>
              </Stat>

              <Stat m="0" p="2" flexBasis={[null, null, null, "50%", "33%"]}>
                <StatLabel fontSize="lg" fontWeight="bold">
                  Cessão de créditos
                </StatLabel>
                <StatNumber whiteSpace="nowrap" color="blue.200">
                  R$ {bigNumber2}
                </StatNumber>
                <StatHelpText color="gray.200">
                  Negociados em cessões de direitos creditórios.
                </StatHelpText>
              </Stat>

              <Stat m="0" p="2" flexBasis={[null, null, null, "50%", "33%"]}>
                <StatLabel fontSize="lg" fontWeight="bold">
                  Captação de recursos
                </StatLabel>
                <StatNumber whiteSpace="nowrap" color="blue.200">
                  R$ {bigNumber5}
                </StatNumber>
                <StatHelpText color="gray.200">
                  Captados em recursos aprovados.
                </StatHelpText>
              </Stat>

              <Stat m="0" p="2" flexBasis={[null, null, null, "50%", "33%"]}>
                <StatLabel fontSize="lg" fontWeight="bold">
                  Benefícios
                </StatLabel>
                <StatNumber whiteSpace="nowrap" color="blue.200">
                  R$ {bigNumber1}
                </StatNumber>
                <StatHelpText color="gray.200">
                  Alcançados em benefícios para nossos clientes.
                </StatHelpText>
              </Stat>

              <Stat m="0" p="2" flexBasis={[null, null, null, "50%", "33%"]}>
                <StatLabel fontSize="lg" fontWeight="bold">
                  Leilões de fazendas
                </StatLabel>
                <StatNumber whiteSpace="nowrap" color="blue.200">
                  R$ {bigNumber4}
                </StatNumber>
                <StatHelpText color="gray.200">
                  Comerciados em leilões de fazendas.
                </StatHelpText>
              </Stat>
            </StatGroup>

            <Stack direction="row" spacing="4" mt="16">
              <Box
                display="flex"
                flexDirection="column"
                justifyContent="space-between"
                flex="1"
                pos="relative"
                pt="16"
                bg="white"
                borderRadius="md"
                shadow="dark-lg"
              >
                <Flex
                  align={[null, null, null, "center", "flex-end"]}
                  pos="absolute"
                  top="-12"
                  w="full"
                  mx="auto"
                  direction={[null, null, null, "column", "row"]}
                >
                  <Image
                    border="8px"
                    borderColor="white"
                    borderRadius="full"
                    boxSize="100px"
                    src="/images/valmir.jpg"
                  />
                  <Flex
                    direction="column"
                    justify="flex-end"
                    ml="4"
                    pr="4"
                    textAlign={[null, null, null, "center", "left"]}
                  >
                    <Heading size="md">Valmir Andrade</Heading>
                    <Text>Fazenda Pontal</Text>
                  </Flex>
                </Flex>
                <Text
                  as="cite"
                  letterSpacing="tighter"
                  fontSize={[null, null, null, "md", "lg"]}
                  lineHeight="shorter"
                  px="4"
                  mt={[null, null, null, "10", "0"]}
                >
                  "Foi muito importante seu trabalho na reestruturação da
                  empresa. Nos pôs de volta no mercado. Então eu tenho muito o
                  que agradecer."
                </Text>
                <AspectRatio
                  w="full"
                  ratio={16 / 9}
                  mt="4"
                  display={["none", null, null, null, "block"]}
                >
                  <iframe
                    title='Firme na lavoura - Aleximar Arantes Gomes, Rancho "K" - Agromentoria'
                    src="https://www.youtube.com/embed/XtxKhfTYHes"
                    allowFullScreen
                  />
                </AspectRatio>
              </Box>
              <Box
                display="flex"
                flexDirection="column"
                justifyContent="space-between"
                flex="1"
                pos="relative"
                pt="16"
                bg="white"
                borderRadius="md"
                shadow="dark-lg"
              >
                <Flex
                  align={[null, null, null, "center", "flex-end"]}
                  pos="absolute"
                  top="-12"
                  w="full"
                  direction={[null, null, null, "column", "row"]}
                >
                  <Image
                    border="8px"
                    borderColor="white"
                    borderRadius="full"
                    boxSize="100px"
                    src="/images/aleximar.jpg"
                  />
                  <Flex
                    direction="column"
                    justify="flex-end"
                    ml="4"
                    pr="4"
                    textAlign={[null, null, null, "center", "left"]}
                  >
                    <Heading size="md">Aleximar Arantes</Heading>
                    <Text>Rancho K</Text>
                  </Flex>
                </Flex>
                <Text
                  as="cite"
                  d="block"
                  letterSpacing="tighter"
                  lineHeight="shorter"
                  fontSize={[null, null, null, "md", "lg"]}
                  px="4"
                  mb={[null, null, null, "4", "0"]}
                  mt={[null, null, null, "10", "0"]}
                >
                  "Pelo excelente trabalho que o Dr. Marco fez e a maneira que
                  nos conduziu, acho que acertamos muito.
                  <br /> A equipe dele trabalhou muito bem. E o que nós
                  conseguimos foi muito bom!"
                </Text>
                <AspectRatio
                  w="full"
                  ratio={16 / 9}
                  mt="4"
                  display={["none", null, null, null, "flex"]}
                >
                  <iframe
                    title='Firme na lavoura - Aleximar Arantes Gomes, Rancho "K" - Agromentoria'
                    src="https://www.youtube.com/embed/2R32c_wznnY"
                    allowFullScreen
                  />
                </AspectRatio>
              </Box>
            </Stack>
          </Flex>
        </Flex>
      </Container>
    </Flex>
  );
};

export default Login;
