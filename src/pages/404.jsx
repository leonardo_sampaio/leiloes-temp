import { Center, Heading } from "@chakra-ui/react";
import { WarningTwoIcon } from "@chakra-ui/icons";

const Custom404 = () => {
  return (
    <>
      <Center flex="1">
        <Heading>
          <WarningTwoIcon mr="4" />
          Página não encontrada
        </Heading>
      </Center>
    </>
  );
};

export default Custom404;
