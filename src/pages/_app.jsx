import Head from "next/head";
import { useRouter } from "next/router";
import HeaderFO from "../components/front-office/Header";
import FooterFO from "../components/front-office/Footer";
import HeaderBO from "../components/back-office/Header";
import FooterBO from "../components/back-office/Footer";
import { ChakraProvider } from "@chakra-ui/react";
import { extendTheme, Flex, Box } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import "../styles/globals.css";
import { parseCookies } from "nookies";

import { UserProvider } from "../context/UserContext";
import { BackOfficeProvider } from "../context/BackOfficeContext";
import { FrontOfficeProvider } from "../context/FrontOfficeContext";

const customTheme = {
  fonts: {
    body: "Rubik, system-ui, sans-serif",
    heading: "Rubik, system-ui, sans-serif",
    mono: "Menlo, monospace",
  },
  colors: {
    brand: {
      500: "#51792c",
    },
  },
  components: {
    Button: {
      variants: {
        "header-menu-item": {
          h: "36px",
          bg: "gray.700",
          px: "4",
          py: "1",
          fontWeight: "normal",
          boxShadow: "base",
          border: "2px",
          borderColor: "transparent",
          borderRadius: "100",
          _hover: { borderColor: "blue.500", textDecoration: "none" },
          _active: { bg: "blue.500" },
        },
      },
    },
  },
};
const theme = extendTheme(customTheme);

const MyApp = ({ Component, pageProps }) => {
  const router = useRouter();
  const isBackOfficeRoute = router.pathname.includes("/back-office");
  return (
    <UserProvider>
      <ChakraProvider theme={theme}>
        <Head>
          <title>Agromentoria</title>
          <meta
            name="viewport"
            content="minimum-scale=1, maximum-scale=1, initial-scale=1, width=device-width"
          />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;700;800&display=swap"
            rel="stylesheet"
          />
        </Head>
        {isBackOfficeRoute ? (
          <BackOfficeProvider>
            <HeaderBO />
            <Box as="main" flex="1" bg="gray.700">
              <Component {...pageProps} />
            </Box>
            <FooterBO />
          </BackOfficeProvider>
        ) : (
          <FrontOfficeProvider>
            <HeaderFO />
            <Box as="main" flex="1" bg="gray.100">
              <Component {...pageProps} />
            </Box>
            <FooterFO />
          </FrontOfficeProvider>
        )}
      </ChakraProvider>
    </UserProvider>
  );
};

export default MyApp;
