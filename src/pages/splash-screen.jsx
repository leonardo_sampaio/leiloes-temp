import { Center, Image } from "@chakra-ui/react";

const SplashScreen = () => {
  return (
    <Center flex="1">
      <Image w={500} fit="contain" src="/images/agromentoria-logo-g.png" />
    </Center>
  );
};

export default SplashScreen;
