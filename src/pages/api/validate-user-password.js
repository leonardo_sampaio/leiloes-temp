export default (req, res) => {
  const { password: providedPassword } = req.body,
    system_password = process.env.NEXT_PUBLIC_FRONTEND_PASSWORD;
  if (providedPassword === system_password) {
    res.status(200).json({
      data: "Senha correta",
    });
  } else {
    res.status(401).json({
      data: "Senha incorreta",
    });
  }
};
