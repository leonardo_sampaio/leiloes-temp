import { Center, Heading, Link } from "@chakra-ui/react";

const Index = () => {
  return (
    <Center flex="1" flexDirection="column">
      <Heading>Front Office Index page</Heading>
      <Link href="/login">
        <a>Go to Login page</a>
      </Link>
    </Center>
  );
};

export default Index;
