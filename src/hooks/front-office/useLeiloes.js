import { useContextSelector } from "use-context-selector";
import { FrontOfficeContext } from "../../context/FrontOfficeContext";

export function useLeiloes() {
  const lotes = useContextSelector(
    FrontOfficeContext,
    (frontOffice) => frontOffice.lotes
  );

  return {
    lotes,
  };
}
