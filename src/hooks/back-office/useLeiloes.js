import { useContextSelector } from "use-context-selector";
import { BackOfficeContext } from "../../context/BackOfficeContext";

export function useLeiloes() {
  const lotes = useContextSelector(
    BackOfficeContext,
    (backOffice) => backOffice.lotes
  );

  return {
    lotes,
  };
}
