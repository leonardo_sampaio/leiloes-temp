import { useContextSelector } from "use-context-selector";
import { UserContext } from "../context/UserContext";

export function useUser() {
  const userSession = useContextSelector(
    UserContext,
    (user) => user.userSession
  );
  const onUserLogin = useContextSelector(
    UserContext,
    (user) => user.onUserLogin
  );
  const onUserLogout = useContextSelector(
    UserContext,
    (user) => user.onUserLogout
  );

  return {
    userSession,
    onUserLogin,
    onUserLogout,
  };
}
