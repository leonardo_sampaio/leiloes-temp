import { useState, useEffect, useCallback } from "react";
import { createContext } from "use-context-selector";

export const BackOfficeContext = createContext();

export const BackOfficeProvider = ({ children }) => {
  const [lotes, setLotes] = useState([1, 2, 3, 4, 5]);
  const [cessoesDeCredito, setCessoesDeCredito] = useState();

  // useEffect(() => {
  //   console.log("cookies[user-session]", cookies["user-session"]);
  //   if (cookies["user-session"]) setUserSession(true);
  // }, []);

  return (
    <BackOfficeContext.Provider
      value={{
        lotes,
        cessoesDeCredito,
      }}
    >
      {children}
    </BackOfficeContext.Provider>
  );
};
