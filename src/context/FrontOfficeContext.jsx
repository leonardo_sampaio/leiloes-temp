import { useState, useEffect, useCallback } from "react";
import { createContext } from "use-context-selector";

export const FrontOfficeContext = createContext();

export const FrontOfficeProvider = ({ children }) => {
  const [lotes, setLotes] = useState([1, 3, 5]);
  const [cessoesDeCredito, setCessoesDeCredito] = useState();

  // useEffect(() => {
  //   console.log("cookies[user-session]", cookies["user-session"]);
  //   if (cookies["user-session"]) setUserSession(true);
  // }, []);

  return (
    <FrontOfficeContext.Provider
      value={{
        lotes,
        cessoesDeCredito,
      }}
    >
      {children}
    </FrontOfficeContext.Provider>
  );
};
