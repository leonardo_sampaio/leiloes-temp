import { useState, useEffect, useCallback } from "react";
import { setCookie, destroyCookie, parseCookies } from "nookies";
import { createContext } from "use-context-selector";

export const UserContext = createContext();

const cookies = parseCookies();

export const UserProvider = ({ children }) => {
  const [userSession, setUserSession] = useState();

  useEffect(() => {
    if (cookies["user-session"]) setUserSession(true);
  }, []);

  const onUserLogin = useCallback(() => {
    const now = new Date().getTime();
    const sessionData = true;
    setCookie(null, "user-session", sessionData, {
      maxAge: 30 * 24 * 60 * 60,
      path: "/",
    });
    setUserSession(sessionData);
  }, []);

  const onUserLogout = useCallback(() => {
    destroyCookie(null, "user-session");
    const sessionData = false;
    setUserSession(sessionData);
  }, []);

  return (
    <UserContext.Provider
      value={{
        userSession,
        onUserLogin,
        onUserLogout,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};
