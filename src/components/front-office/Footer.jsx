import Image from "next/image";
import Link from "next/link";
import { Flex, Image as Imagey } from "@chakra-ui/react";

const Footer = () => {
  return (
    <Flex
      as="footer"
      justify="center"
      bg="gray.200"
      borderTopRadius="3xl"
      boxShadow="dark-lg"
      //   mx={1}
    >
      <Link href="/splash-screen">
        <Imagey
          // opacity=".85"
          fit="cover"
          src="/images/agromentoria-logo-h-bw.png"
        />
      </Link>
    </Flex>
  );
};

export default Footer;
