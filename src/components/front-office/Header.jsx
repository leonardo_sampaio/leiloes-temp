import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import {
  Flex,
  Stack,
  VStack,
  Wrap,
  WrapItem,
  Heading,
  Text,
  Button,
  ButtonGroup,
  Input,
  Avatar,
  Icon,
  Image,
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  useDisclosure,
} from "@chakra-ui/react";

import { useUser } from "../../hooks/useUser";
import PasswordModal from "../../components/PasswordModal.jsx";

const Header = () => {
  const { userSession, onUserLogin, onUserLogout } = useUser();
  const [displayPasswordModal, setDisplayPasswordModal] = useState(false);

  const router = useRouter();

  function handleLogout() {
    onUserLogout();
    router.push("/");
  }
  function handleLogin() {
    setDisplayPasswordModal(true);
    setTimeout(() => {
      setDisplayPasswordModal(false);
    }, 0);
  }

  const isThatCurrentPath = (path) => router.pathname === path;

  return (
    <>
      <PasswordModal open={displayPasswordModal} />
      <Flex
        as="header"
        bg="gray.200"
        h={14}
        p={1}
        borderBottom="1px"
        borderBottomColor="gray.900"
        alignItems="center"
        color="black"
      >
        <Link href="/">
          <a>
            <Image
              w={200}
              ml={2}
              fit="contain"
              src="/images/agromentoria-logo-h-g.png"
            />
          </a>
        </Link>
        <ButtonGroup flex="1" ml="4">
          <Link href="/escritorio-digital">
            <Button
              isDisabled={true}
              variant="header-menu-item"
              bg={
                isThatCurrentPath("/escritorio-digital")
                  ? "blue.500"
                  : "gray.300"
              }
            >
              Escritório digital
            </Button>
          </Link>
          <Link href="/leiloes">
            <Button
              variant="header-menu-item"
              bg={isThatCurrentPath("/leiloes") ? "blue.500" : "gray.300"}
            >
              Leilões
            </Button>
          </Link>
          <Link href="/cessao-de-creditos">
            <Button
              isDisabled={true}
              variant="header-menu-item"
              bg={
                isThatCurrentPath("/cessao-de-creditos")
                  ? "blue.500"
                  : "gray.300"
              }
            >
              Cessão de créditos
            </Button>
          </Link>
          <Link href="/insumos">
            <Button
              isDisabled={true}
              variant="header-menu-item"
              bg={isThatCurrentPath("/insumos") ? "blue.500" : "gray.300"}
            >
              Insumos
            </Button>
          </Link>
        </ButtonGroup>
        <Link
          href="/back-office"
          // mx="4"
        >
          Back Office
        </Link>
        <Link
          href="/back-office/leiloes"
          // mx="4"
        >
          Leiloes Back Office
        </Link>
        {userSession ? (
          <Link href="/logout">
            <a>Logout</a>
          </Link>
        ) : (
          <Link href="/login">
            <a>Login</a>
          </Link>
        )}
      </Flex>
    </>
  );
};

export default Header;
