import React, { useEffect, useState } from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  useDisclosure,
} from "@chakra-ui/react";
import { CheckIcon } from "@chakra-ui/icons";
import { Formik, Form, Field, ErrorMessage } from "formik";

import { useUser } from "../hooks/useUser";

function PasswordModal({ open }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [validated, setValidated] = useState(false);
  const [isMounted, setIsMounted] = useState(null);
  const initialRef = React.useRef();
  const { onUserLogin } = useUser();

  useEffect(() => {
    setIsMounted(true);
    return () => setIsMounted(false);
  }, []);

  useEffect(() => {
    if (open && isMounted) {
      onOpen();
    }
  }, [open]);

  const validateUserPassword = (password) => {
    return fetch("/api/validate-user-password", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        password,
      }),
    });
  };

  return (
    <>
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        initialFocusRef={initialRef}
        // closeOnEsc={false}
        // closeOnOverlayClick={false}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalCloseButton />
          <ModalHeader>Acesso restrito</ModalHeader>
          <ModalBody pb={6}>
            <Formik
              initialValues={{ password: "" }}
              // validate={(values) => ({ password: "errado!!" })}
              onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
                if (validated) return null;
                setTimeout(async () => {
                  const validatePw = await validateUserPassword(
                    values.password
                  );
                  if (validatePw.status === 200) {
                    setSubmitting(false);
                    setValidated(true);
                    onUserLogin();
                    setTimeout(() => {
                      onClose(true);
                    }, 750);
                  } else if (validatePw.status === 401) {
                    setErrors({ password: "Senha incorreta" });
                    setSubmitting(false);
                  }
                }, 750);
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
              }) => (
                <form onSubmit={handleSubmit}>
                  <Field type="password" name="password">
                    {({ field, form }) => (
                      <FormControl
                        isInvalid={
                          form.errors.password && form.touched.password
                        }
                      >
                        <FormLabel htmlFor="password">Senha</FormLabel>
                        <Input
                          {...field}
                          type="password"
                          id="password"
                          ref={initialRef}
                        />
                        <FormErrorMessage>{errors.password}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>
                  <Button
                    mt={4}
                    colorScheme={validated ? "green" : "blue"}
                    isLoading={isSubmitting}
                    loadingText="Validando"
                    rightIcon={validated && <CheckIcon />}
                    isDisabled={!values.password && !isSubmitting}
                    type="submit"
                  >
                    {validated ? "Logado" : "Entrar"}
                  </Button>
                </form>
              )}
            </Formik>
          </ModalBody>
          <ModalFooter></ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
export default PasswordModal;
